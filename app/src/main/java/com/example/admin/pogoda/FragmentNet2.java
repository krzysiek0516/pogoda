package com.example.admin.pogoda;

import android.app.Fragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

public class FragmentNet2 extends Fragment {
    View view;
    private TextView textViewBrakNeta,textview,textview2,textview3,textview4,textviewTytul;
    private ImageView imgView;
    String url, miasto, strona2;
    Context context = getActivity();
    //SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);

    public FragmentNet2(){}


    public void horizontal(String q){
        miasto = q;
        //miasto = "\t\tŁódź";
        boolean isNetAvailable = isNetworkAvailable();//sprawdzanie polaczenia z internetem
        System.out.println("\nPolaczenie jest " + isNetAvailable);

        try {
            url = "http://api.openweathermap.org/data/2.5/weather?q="+ URLEncoder.encode(miasto.substring(2),"utf-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            //url = "http://api.openweathermap.org/data/2.5/weather?q="+ miasto.substring(2);
        }


        textview = (TextView) view.findViewById(R.id.temp);
        textview2 = (TextView) view.findViewById(R.id.spead);
        textview3 = (TextView) view.findViewById(R.id.pressure);
        textview4 = (TextView) view.findViewById(R.id.clouds);
        textviewTytul = (TextView) view.findViewById(R.id.tytul);
        textViewBrakNeta = (TextView) view.findViewById(R.id.brakNeta);
        imgView = (ImageView) view.findViewById(R.id.obrazek);

        if (isNetAvailable) {
            textviewTytul.setText(miasto);
            textViewBrakNeta.setText("");
            MyRun myRun = new MyRun(url);
            Thread watek = new Thread(myRun);
            watek.start();
            try {
                watek.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            strona2 = myRun.get();

            try {
                InternalStorage.writeObject(this.getActivity(),"jstring", strona2);

            } catch (IOException e) {
                e.printStackTrace();
            }

            jsonowanie(strona2);

        }else{
            textViewBrakNeta.setText("Brak Internetu. \nOstatnio pobrane dane:");
            try {
                // Retrieve the file from internal storage
                String jstring = (String)InternalStorage.readObject(this.getActivity().getApplicationContext(), "jstring");

                Bitmap bm = (Bitmap)InternalStorage.readObject(this.getActivity(),"obrazek");
                imgView.setImageBitmap(bm);
                System.out.println("To jest strona ktora idzie jak ni ma neta: " + jstring + " KONIEC");
                jsonowanie(jstring);
            } catch (IOException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        }
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.pogodanet, container, false);
        return view;
    }

    public boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager = (ConnectivityManager)getActivity().getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null;
    }

    public void jsonowanie(String strona2){
        if (strona2 != "") {
            JSONObject strona;
            String urlObrazka;
            try {
                strona = new JSONObject(strona2);

                JSONArray arr = strona.getJSONArray("weather");

                String pageName2 = arr.getJSONObject(0).getString("description");

                String pageName3 = strona.getJSONObject("main").getString("temp");
                String pageName4 = strona.getJSONObject("main").getString("pressure");
                String pageName5 = strona.getJSONObject("wind").getString("speed");
                String nrObrazka = arr.getJSONObject(0).getString("icon");

                //textviewTytul.setText(city);
                textview.setText("Weather: " + pageName2);
                textview2.setText("Temp: " + String.format("%.2f", Double.parseDouble(pageName3) - 273) + " °C");
                textview3.setText("Presure: " + pageName4 + " hP");
                textview4.setText("Wind: " + pageName5 + " km/h");

                urlObrazka = "http://openweathermap.org/img/w/" + nrObrazka + ".png";
                ImageFromUrl imageFromUrl = new ImageFromUrl(urlObrazka);
                Thread watek = new Thread(imageFromUrl);
                watek.start();
                try {
                    watek.join();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                Bitmap bm = imageFromUrl.get();
                if (bm != null) {
                    System.out.println(bm);
                    imgView.setImageBitmap(bm);
                    try {
                        InternalStorage.writeObject(this.getActivity(),"obrazek",bm);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

            } catch (JSONException e) {
                e.printStackTrace();
                textview2.setText("City not found. Sorry");
            }
        } else {
            textview.setText("Coś nie tak z danymi...\nSpróbuj ponownie");
            textview2.setText("");
            textview3.setText("");
            textview4.setText("");
            imgView.setImageBitmap(null);

        }
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState){
        super.onActivityCreated(savedInstanceState);
        if(savedInstanceState!=null)
            this.miasto = savedInstanceState.getString("miasto");
        //super.onRestoreInstanceState(savedInstanceState);
    }

}
