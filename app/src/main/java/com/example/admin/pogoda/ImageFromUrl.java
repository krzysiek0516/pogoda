package com.example.admin.pogoda;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;


public class ImageFromUrl implements Runnable {

    String adres;
    Bitmap bm = null;

    public ImageFromUrl(String adres){
        this.adres = adres;
    }
    @Override
    public void run() {
        URLConnection conn;

        try {
            URL aURL = new URL(adres);
            conn = aURL.openConnection();
            conn.connect();
            InputStream is = conn.getInputStream();
            BufferedInputStream bis = new BufferedInputStream(is);
            bm = BitmapFactory.decodeStream(bis);

            bis.close();
            is.close();
        } catch (IOException e) {
            System.out.println("Error getting the image from server : " + e.getMessage().toString());
        }
    }
    public Bitmap get(){
        return bm;
    }


}
