package com.example.admin.pogoda;

import android.app.Fragment;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class FragmentLista extends Fragment {
    View view;
    
    List<Kontynent> kontynenty = new ArrayList<>();
    List<String> lista = new ArrayList<>();
    ListView listView;
    String element;
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.activity_pogoda, container, false);
        if (lista.isEmpty()) {
            stworz();

            for (int i = 0; i < kontynenty.size(); i++) {
                lista.add(kontynenty.get(i).nazwa);
            }
        }
        listView = (ListView) view.findViewById(R.id.miLista);
        listView.setAdapter(new listaAdapter(lista));


        listView.setOnItemClickListener(new android.widget.AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(android.widget.AdapterView<?> parent, View view, int position, long id) {
                element = lista.get(position);
                lista.clear();
                addKontynenty(element);
                listView.setAdapter(new listaAdapter(lista));
            }
        });
        return view;
    }
    private void zmianaWidoku(){
        if(getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
            FragmentNet2 fragment = (FragmentNet2) getFragmentManager().findFragmentById(R.id.fragment2);
            fragment.horizontal(element);
        }
            else{
                getFragmentManager().beginTransaction()
                        .replace(R.id.container, new FragmentNet(element)).addToBackStack(null)
                        .commit();
            }
}
    class listaAdapter extends BaseAdapter {
        private List<String> elementy;

        public listaAdapter(List<String> lista) {
            elementy = lista;
        }

        @Override
        public int getCount() {
            return elementy.size();
        }

        @Override
        public String getItem(int position) {
            return elementy.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            TextView result;
            if (convertView == null) {
                result = new TextView(parent.getContext());
                result.setText(elementy.get(position));
            } else {
                result = (TextView) convertView;
                result.setText(elementy.get(position));
            }
            return result;

        }

    }

    private void addMiasta(int i,int j,String element){
        for (int k = 0; k < kontynenty.get(i).panstwa.get(j).miasta.size(); k++) {
            lista.add("\t\t"+kontynenty.get(i).panstwa.get(j).miasta.get(k).nazwa);
            if (element.equals("\t\t"+kontynenty.get(i).panstwa.get(j).miasta.get(k).nazwa)) {
                zmianaWidoku();
            }
        }
    }
    private void addPanstwa(int i,String element){
        for (int j = 0; j < kontynenty.get(i).panstwa.size(); j++) {
            lista.add("\t"+kontynenty.get(i).panstwa.get(j).nazwa);

            if (kontynenty.get(i).panstwa.get(j).rozwiniente) {
                if (element.equals("\t"+kontynenty.get(i).panstwa.get(j).nazwa)) {
                    kontynenty.get(i).panstwa.get(j).setRozwinienteFalse();
                    continue;
                }
                else {
                    addMiasta(i,j,element);
                }
            }
            else{
                if (element.contains(kontynenty.get(i).panstwa.get(j).nazwa)) {
                    kontynenty.get(i).panstwa.get(j).setRozwinienteTrue();
                    addMiasta(i,j,element);
                }
            }
        }
    }
    private void addKontynenty(String element){
        for (int i = 0; i < kontynenty.size(); i++) {
            lista.add(kontynenty.get(i).nazwa);
            if (kontynenty.get(i).rozwiniente) {
                if (kontynenty.get(i).nazwa==element) {
                    kontynenty.get(i).setRozwinienteFalse();
                    continue;
                }
                else {
                    addPanstwa(i, element);
                }
            }
            else {
                if (kontynenty.get(i).nazwa==element) {
                    kontynenty.get(i).setRozwinienteTrue();
                    addPanstwa(i,element);
                }
            }
        }
    }
    private void stworz(){
        kontynenty.add(new Kontynent("Europa"));
        kontynenty.add(new Kontynent("Azja"));
        kontynenty.add(new Kontynent("Ameryka Północna"));

        kontynenty.get(0).addPanstwo("Polska");
        kontynenty.get(0).addPanstwo("Francja");
        kontynenty.get(0).addPanstwo("Niemcy");
        kontynenty.get(0).addPanstwo("Hiszpania");

        kontynenty.get(1).addPanstwo("Chiny");
        kontynenty.get(1).addPanstwo("Indie");
        kontynenty.get(1).addPanstwo("Japonia");
        kontynenty.get(1).addPanstwo("Rosja");

        kontynenty.get(2).addPanstwo("USA");
        kontynenty.get(2).addPanstwo("Kanada");
        kontynenty.get(2).addPanstwo("Meksyk");

        kontynenty.get(0).panstwa.get(0).addMiasto("Łódź");
        kontynenty.get(0).panstwa.get(0).addMiasto("Warszawa");
        kontynenty.get(0).panstwa.get(0).addMiasto("Kraków");
        kontynenty.get(0).panstwa.get(0).addMiasto("Sieradz");

        kontynenty.get(0).panstwa.get(1).addMiasto("Paryż");
        kontynenty.get(0).panstwa.get(1).addMiasto("Marsylia");
        kontynenty.get(0).panstwa.get(1).addMiasto("Lyon");
        kontynenty.get(0).panstwa.get(1).addMiasto("Niort");

        kontynenty.get(0).panstwa.get(2).addMiasto("Berlin");
        kontynenty.get(0).panstwa.get(2).addMiasto("Hamburg");
        kontynenty.get(0).panstwa.get(2).addMiasto("Brema");
        kontynenty.get(0).panstwa.get(2).addMiasto("Kolonia");

        kontynenty.get(0).panstwa.get(3).addMiasto("Madryt");
        kontynenty.get(0).panstwa.get(3).addMiasto("Kordoba");
        kontynenty.get(0).panstwa.get(3).addMiasto("Malaga");
        kontynenty.get(0).panstwa.get(3).addMiasto("Saragossa");

        kontynenty.get(1).panstwa.get(0).addMiasto("Hong Kong");
        kontynenty.get(1).panstwa.get(0).addMiasto("Kanton");
        kontynenty.get(1).panstwa.get(0).addMiasto("Wuhan");
        kontynenty.get(1).panstwa.get(0).addMiasto("Chengdu");

        kontynenty.get(1).panstwa.get(1).addMiasto("Surat");
        kontynenty.get(1).panstwa.get(1).addMiasto("Bangalore");
        kontynenty.get(1).panstwa.get(1).addMiasto("Madras");
        kontynenty.get(1).panstwa.get(1).addMiasto("Agra");

        kontynenty.get(1).panstwa.get(2).addMiasto("Osaka");
        kontynenty.get(1).panstwa.get(2).addMiasto("Sapporo");
        kontynenty.get(1).panstwa.get(2).addMiasto("Akita");
        kontynenty.get(1).panstwa.get(2).addMiasto("Hiroszima");

        kontynenty.get(1).panstwa.get(3).addMiasto("Moskwa");
        kontynenty.get(1).panstwa.get(3).addMiasto("Kazań");
        kontynenty.get(1).panstwa.get(3).addMiasto("Saratów");
        kontynenty.get(1).panstwa.get(3).addMiasto("Omsk");

        kontynenty.get(2).panstwa.get(0).addMiasto("Nowy Jork");
        kontynenty.get(2).panstwa.get(0).addMiasto("Waszyngton");
        kontynenty.get(2).panstwa.get(0).addMiasto("Miami");
        kontynenty.get(2).panstwa.get(0).addMiasto("Las Vegas");

        kontynenty.get(2).panstwa.get(1).addMiasto("Edmonton");
        kontynenty.get(2).panstwa.get(1).addMiasto("Calgary");
        kontynenty.get(2).panstwa.get(1).addMiasto("Ottawa");
        kontynenty.get(2).panstwa.get(1).addMiasto("Montreal");

        kontynenty.get(2).panstwa.get(2).addMiasto("Meksyk");
        kontynenty.get(2).panstwa.get(2).addMiasto("Monterrey");
        kontynenty.get(2).panstwa.get(2).addMiasto("Guadalajara");
        kontynenty.get(2).panstwa.get(2).addMiasto("Tijuana");
    }

}