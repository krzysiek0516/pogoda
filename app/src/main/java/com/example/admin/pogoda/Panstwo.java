package com.example.admin.pogoda;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Admin on 2015-04-14.
 */
public class Panstwo {
    String nazwa;
    boolean rozwiniente=false;
    List<Miasto> miasta = new ArrayList<Miasto>();

    public Panstwo(String N) {
        nazwa = N;
    }

    void addMiasto(String N) {
        miasta.add(new Miasto(N));
    }
    public void setRozwinienteTrue(){
        rozwiniente = true;
    }
    public void setRozwinienteFalse(){
        rozwiniente = false;
    }
}