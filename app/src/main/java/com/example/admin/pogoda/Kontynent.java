package com.example.admin.pogoda;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Admin on 2015-04-14.
 */
public class Kontynent {
    String nazwa;
    boolean rozwiniente=false;
    List<Panstwo> panstwa;

    public Kontynent(String N) {
        nazwa = N;
        panstwa = new ArrayList<Panstwo>();
    }

    void addPanstwo(String N) {
        panstwa.add(new Panstwo(N));
    }
    public void setRozwinienteTrue(){
        rozwiniente = true;
    }
    public void setRozwinienteFalse(){
        rozwiniente = false;
    }

}
